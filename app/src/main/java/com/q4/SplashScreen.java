package com.q4;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.q4.login.LoginScreen;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by promatics on 26/11/15.
 */
public class SplashScreen extends Fragment {
    private Timer timer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.splash_screen, container, false);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, new LoginScreen()).commit();
            }
        }, 2000);
    }

    @Override
    public void onStop() {
        super.onStop();
        timer.cancel();
    }
}
