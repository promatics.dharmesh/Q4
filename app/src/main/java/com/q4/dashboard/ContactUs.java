package com.q4.dashboard;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.q4.R;
import com.q4.utils.CallService;
import com.q4.utils.CommonUtils;
import com.q4.utils.Constants;
import com.q4.utils.ServiceCallback;
import com.q4.utils.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ContactUs extends Fragment implements ServiceCallback {
    private EditText etSupportEmail, etSubject, etQuery;
    private Button btnSubmit;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.contact_us, container, false);
        getActivity().setTitle("Contact Us");
        etSupportEmail = (EditText) v.findViewById(R.id.etSupportEmail);
        etSubject = (EditText) v.findViewById(R.id.etSubject);
        etQuery = (EditText) v.findViewById(R.id.etQuery);
        btnSubmit = (Button) v.findViewById(R.id.btnSubmit);
        etSupportEmail.setText("support@q4solutions.com");
        etSupportEmail.setEnabled(false);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSubject.getText().toString().trim().isEmpty()) {
                    etSubject.setError("Please enter subject");
                    etSubject.requestFocus();
                } else if (etQuery.getText().toString().trim().isEmpty()) {
                    etQuery.setError("Please enter your query");
                    etQuery.requestFocus();
                } else {
                    etSubject.setError(null);
                    etQuery.setError(null);
                    callContactUs();
                }
            }
        });
        return v;
    }

    private void callContactUs() {
        HashMap<String, String> map = new HashMap<>();
        map.put("email", User.getInstance().getEmail());
        map.put("subject", etSubject.getText().toString());
        map.put("query", etQuery.getText().toString());
        map.put("send", "Contact");
        new CallService(this, getActivity(), Constants.REQ_CONTACT_US, map).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.CONTACT_US);
    }

    @Override
    public void onServiceResponse(int requestcode, JSONObject response) {
        switch (requestcode) {
            case Constants.REQ_CONTACT_US:
                try {
                    int code = response.getInt("status");
                    if (code == 1) {
                        CommonUtils.showToast1(getActivity(), "Your query has been successfully submitted");
                        etSubject.setText("");
                        etQuery.setText("");
                    } else {
                        CommonUtils.showToast1(getActivity(), "Error Occurred.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(getActivity(), "Invalid response from server");
                }
                break;
        }
    }
}
