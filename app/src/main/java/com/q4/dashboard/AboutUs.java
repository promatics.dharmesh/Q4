package com.q4.dashboard;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.q4.R;
import com.q4.utils.CallService;
import com.q4.utils.CommonUtils;
import com.q4.utils.Constants;
import com.q4.utils.ServiceCallback;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by promatics on 27/11/15.
 */
public class AboutUs extends Fragment implements ServiceCallback {
    private TextView txtTitle, txtDesc;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.about_us, container, false);
        getActivity().setTitle("About Us");
        txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        txtDesc = (TextView) v.findViewById(R.id.txtDescription);
        new CallService(this, getActivity(), Constants.REQ_ABOUT_US).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.ABOUT_US);
        return v;
    }

    @Override
    public void onServiceResponse(int requestcode, JSONObject response) {
        switch (requestcode) {
            case Constants.REQ_ABOUT_US:
                try {
                    int code = response.getInt("status");
                    if (code == 1) {
                        txtTitle.setText(response.getJSONObject("data").getJSONObject("Cmspage").getString("title"));
                        txtDesc.setText(response.getJSONObject("data").getJSONObject("Cmspage").getString("description"));
                    } else {
                        CommonUtils.showToast1(getActivity(), "error occurred.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(getActivity(), "Invalid response from server.");
                }
                break;
        }
    }
}
