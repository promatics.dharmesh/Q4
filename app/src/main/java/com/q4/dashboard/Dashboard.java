package com.q4.dashboard;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.SearchView;
import android.widget.TextView;

import com.q4.R;

import java.lang.reflect.Field;

/**
 * Created by promatics on 26/11/15.
 */
public class Dashboard extends AppCompatActivity {
    private Toolbar toolbar;
    public static DrawerLayout Drawer;
    ActionBarDrawerToggle mDrawerToggle;
    private FragmentManager fm;
    private String title;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.dashboard);
            toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);
            Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
            fm = getSupportFragmentManager();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.app_name, R.string.app_name) {

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    supportInvalidateOptionsMenu();
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    supportInvalidateOptionsMenu();
                }
            }; // Drawer Toggle Object Made
            Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
            mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State
            try {
                if (savedInstanceState == null) {
                    beginTransactions(R.id.content_frame, new HomeScreen(),
                            "Home Screen");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean isOpen = Drawer.isDrawerOpen(Gravity.LEFT);
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setVisible(!isOpen);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        final int textViewID = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        final AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(textViewID);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (Drawer.isDrawerOpen(Gravity.LEFT))
                    Drawer.closeDrawers();
                else
                    Drawer.openDrawer(Gravity.LEFT);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        try {
            if (Drawer.isDrawerOpen(Gravity.LEFT)) {
                Drawer.closeDrawers();
                return;
            }
            HomeScreen frg = (HomeScreen) getSupportFragmentManager().findFragmentByTag("Home Screen");
            if (frg.isVisible()) {
                finish();
            } else if (fm.getBackStackEntryCount() == 1) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void beginTransactions(int id, Fragment frag, String tag) {
        Fragment prevfrag = getSupportFragmentManager().findFragmentByTag(tag);
        fm = getSupportFragmentManager();
        if (null == prevfrag) {
            FragmentTransaction ft = fm.beginTransaction();
            // ft.setCustomAnimations(R.anim.left_to_right, 0, 0,
            // R.anim.right_to_left);
            ft.replace(id, frag, tag);
            ft.addToBackStack(tag);
            ft.commit();
        } else {
            fm.popBackStack(tag, 0);
        }
    }

    /**
     * Hides the soft keyboard.
     */
    public void hidekeyboard(Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            if (((Activity) context).getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(((Activity) context)
                        .getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
