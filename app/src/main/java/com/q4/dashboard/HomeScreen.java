package com.q4.dashboard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.q4.Q1.Q1Screen;
import com.q4.Q2.Q2Screen;
import com.q4.Q3.Q3Screen;
import com.q4.Q4.Q4Screen;
import com.q4.R;

public class HomeScreen extends Fragment implements View.OnClickListener {
    private ImageView imgQ1, imgQ2, imgQ3, imgQ4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_screen, container, false);
        getActivity().setTitle("Home Screen");
        setHasOptionsMenu(true);

        imgQ1 = (ImageView) v.findViewById(R.id.imgq1);
        imgQ2 = (ImageView) v.findViewById(R.id.imgq2);
        imgQ3 = (ImageView) v.findViewById(R.id.imgq3);
        imgQ4 = (ImageView) v.findViewById(R.id.imgq4);

        imgQ1.setOnClickListener(this);
        imgQ2.setOnClickListener(this);
        imgQ3.setOnClickListener(this);
        imgQ4.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgq1:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q1Screen(), Q1Screen.class.getSimpleName());
                break;

            case R.id.imgq2:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q2Screen(), Q2Screen.class.getSimpleName());
                break;

            case R.id.imgq3:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q3Screen(), Q3Screen.class.getSimpleName());
                break;

            case R.id.imgq4:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q4Screen(), Q4Screen.class.getSimpleName());
                break;
        }
    }
}
