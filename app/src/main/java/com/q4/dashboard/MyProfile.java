package com.q4.dashboard;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.q4.R;
import com.q4.utils.CallService;
import com.q4.utils.CommonUtils;
import com.q4.utils.Constants;
import com.q4.utils.ServiceCallback;
import com.q4.utils.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by promatics on 27/11/15.
 */
public class MyProfile extends Fragment implements View.OnClickListener, ServiceCallback {
    private EditText etFName, etLName, etMob, etEmail, etAddress, etAboutMe;
    private Button btnSave;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_profile, container, false);
        getActivity().setTitle("My Profile");
        etFName = (EditText) v.findViewById(R.id.etFName);
        etLName = (EditText) v.findViewById(R.id.etLName);
        etMob = (EditText) v.findViewById(R.id.etMob);
        etEmail = (EditText) v.findViewById(R.id.etEmail);
        etAddress = (EditText) v.findViewById(R.id.etAddress);
        etAboutMe = (EditText) v.findViewById(R.id.etAboutMe);
        btnSave = (Button) v.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        etFName.setText(User.getInstance().getFname());
        etLName.setText(User.getInstance().getLname());
        if (!User.getInstance().getMobNo().equals("null")) {
            etMob.setText(User.getInstance().getMobNo());
        }
        etEmail.setText(User.getInstance().getEmail());
        if (!User.getInstance().getAddress().equals("null"))
            etAddress.setText(User.getInstance().getAddress());
        if (!User.getInstance().getAboutMe().equals("null"))
            etAboutMe.setText(User.getInstance().getAboutMe());
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSave:
                if (etFName.getText().toString().trim().isEmpty()) {
                    etFName.setError("Please enter your first name");
                    etFName.requestFocus();
                } else if (etLName.getText().toString().trim().isEmpty()) {
                    etLName.setError("Please enter your last name");
                    etLName.requestFocus();
                } else if (!etMob.getText().toString().matches(Patterns.PHONE.pattern())) {
                    etMob.setError("Please enter your mobile no.");
                    etMob.requestFocus();
                } else if (!etEmail.getText().toString().matches(Patterns.EMAIL_ADDRESS.pattern())) {
                    etEmail.setError("Please enter your valid email address");
                    etEmail.requestFocus();
                } else if (etAddress.getText().toString().trim().isEmpty()) {
                    etAddress.setError("Please enter your address");
                    etAddress.requestFocus();
                } else if (etAboutMe.getText().toString().trim().isEmpty()) {
                    etAboutMe.setError("Please enter about yourself");
                    etAboutMe.requestFocus();
                } else {
                    etFName.setError(null);
                    etLName.setError(null);
                    etMob.setError(null);
                    etEmail.setError(null);
                    etAddress.setError(null);
                    etAboutMe.setError(null);
                    updateProfile();
                }
                break;
        }
    }

    private void updateProfile() {
        HashMap<String, String> map = new HashMap<>();
        map.put("userId", User.getInstance().getUserId());
        map.put("email", etEmail.getText().toString());
        map.put("firstname", etFName.getText().toString());
        map.put("lastname", etLName.getText().toString());
        map.put("phone", etMob.getText().toString());
        map.put("address", etAddress.getText().toString());
        map.put("about_me", etAboutMe.getText().toString());
        map.put("submit", "update");
        new CallService(this, getActivity(), Constants.REQ_UPDATE_PROFILE, map).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.UPDATE_PROFILE);
    }

    @Override
    public void onServiceResponse(int requestcode, JSONObject response) {
        switch (requestcode) {
            case Constants.REQ_UPDATE_PROFILE:
                try {
                    int code = response.getInt("status");
                    if (code == 1) {
                        JSONObject obj = response.optJSONObject("member_data");
                        String userId = obj.optString("id");
                        String fname = obj.optString("firstname");
                        String lname = obj.optString("lastname");
                        String address = obj.optString("address");
                        String phone = obj.optString("phone");
                        String email = obj.optString("email");
                        String about_me = obj.optString("about_me");
                        User.storeUserData(userId, fname, lname, email, phone, address, about_me);
                        CommonUtils.showToast1(getActivity(), "Your profile has been successfully updated.");
                    } else {
                        String msg = response.getString("msg");
                        CommonUtils.showDialog(getActivity(), msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showToast1(getActivity(), "Invalid response from server");
                }
                break;
        }
    }
}
