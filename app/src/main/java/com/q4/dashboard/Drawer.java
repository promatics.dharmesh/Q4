package com.q4.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.q4.MainActivity;
import com.q4.R;
import com.q4.utils.User;

/**
 * Created by promatics on 26/11/15.
 */
public class Drawer extends Fragment implements View.OnClickListener {
    private TextView txtSearch, txtMyAccount, txtShareApp, txtAboutUs, txtPrivacy, txtContactUs, txtLogout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_drawer, container, false);
        txtSearch = (TextView) v.findViewById(R.id.txtSearch);
        txtMyAccount = (TextView) v.findViewById(R.id.txtMyAccount);
        txtShareApp = (TextView) v.findViewById(R.id.txtShareApp);
        txtAboutUs = (TextView) v.findViewById(R.id.txtAboutUs);
        txtPrivacy = (TextView) v.findViewById(R.id.txtPrivacy);
        txtContactUs = (TextView) v.findViewById(R.id.txtContactUs);
        txtLogout = (TextView) v.findViewById(R.id.txtLogout);

        txtSearch.setOnClickListener(this);
        txtMyAccount.setOnClickListener(this);
        txtShareApp.setOnClickListener(this);
        txtAboutUs.setOnClickListener(this);
        txtPrivacy.setOnClickListener(this);
        txtContactUs.setOnClickListener(this);
        txtLogout.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtSearch:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new HomeScreen(), "Home Screen");
                break;
            case R.id.txtMyAccount:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new MyProfile(), MyProfile.class.getSimpleName());
                break;
            case R.id.txtShareApp:
                Dashboard.Drawer.closeDrawers();
                break;
            case R.id.txtAboutUs:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new AboutUs(), AboutUs.class.getSimpleName());
                break;
            case R.id.txtPrivacy:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Privacy(), Privacy.class.getSimpleName());
                break;
            case R.id.txtContactUs:
                Dashboard.Drawer.closeDrawers();
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new ContactUs(), ContactUs.class.getSimpleName());
                break;
            case R.id.txtLogout:
                Dashboard.Drawer.closeDrawers();
                User.getInstance().logout();
                Intent i = new Intent(getActivity(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                getActivity().finish();
                break;
        }
    }
}
