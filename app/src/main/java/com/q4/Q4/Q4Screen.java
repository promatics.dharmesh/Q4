package com.q4.Q4;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.q4.Q3.Q3Meeting;
import com.q4.Q3.Q3Working;
import com.q4.R;
import com.q4.dashboard.Dashboard;

/**
 * Created by promatics on 26/11/15.
 */
public class Q4Screen extends Fragment implements View.OnClickListener {
    private TextView txtworkQ4, txtMeetingQ4;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q4_screen, container, false);
        getActivity().setTitle("Q4");
        txtMeetingQ4 = (TextView) v.findViewById(R.id.txtMeetingQ4);
        txtworkQ4 = (TextView) v.findViewById(R.id.txtworkQ4);
        txtMeetingQ4.setOnClickListener(this);
        txtworkQ4.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtMeetingQ4:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q4Meeting(), Q4Meeting.class.getSimpleName());
                break;
            case R.id.txtworkQ4:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q4Working(), Q4Working.class.getSimpleName());
                break;
        }

    }
}
