package com.q4.Q4;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.q4.R;

/**
 * Created by promatics on 27/11/15.
 */
public class Q4MeetingEstablish extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q4_establish_meeting, container, false);
        getActivity().setTitle("Meeting with Q4");
        return v;
    }
}
