package com.q4.Q1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.q4.R;
import com.q4.dashboard.Dashboard;

/**
 * Created by promatics on 26/11/15.
 */
public class Q1Meeting extends Fragment implements View.OnClickListener {
    private TextView txtEstablish, txtGather, txtDeternmine, txtClarify, txtFacilitate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q1_meeting, container, false);
        getActivity().setTitle("Meeting with Q1");
        txtEstablish = (TextView) v.findViewById(R.id.txtEstablish);
        txtGather = (TextView) v.findViewById(R.id.txtGather);
        txtDeternmine = (TextView) v.findViewById(R.id.txtDeternmine);
        txtClarify = (TextView) v.findViewById(R.id.txtClarify);
        txtFacilitate = (TextView) v.findViewById(R.id.txtFacilitate);
        txtEstablish.setOnClickListener(this);
        txtGather.setOnClickListener(this);
        txtDeternmine.setOnClickListener(this);
        txtClarify.setOnClickListener(this);
        txtFacilitate.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtEstablish:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q1MeetingEstablish(), Q1MeetingEstablish.class.getSimpleName());
                break;
            case R.id.txtGather:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q1MeetingGather(), Q1MeetingGather.class.getSimpleName());
                break;
            case R.id.txtDeternmine:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q1MeetingDetermine(), Q1MeetingDetermine.class.getSimpleName());
                break;
            case R.id.txtClarify:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q1MeetingClarify(), Q1MeetingClarify.class.getSimpleName());
                break;
            case R.id.txtFacilitate:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q1MeetingFaciliate(), Q1MeetingFaciliate.class.getSimpleName());
                break;
        }
    }
}
