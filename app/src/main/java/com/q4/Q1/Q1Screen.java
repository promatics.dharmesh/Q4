package com.q4.Q1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.q4.R;
import com.q4.dashboard.Dashboard;

/**
 * Created by promatics on 26/11/15.
 */
public class Q1Screen extends Fragment implements View.OnClickListener {
    private TextView txtworkQ1, txtMeetingQ1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q1_screen, container, false);
        txtworkQ1 = (TextView) v.findViewById(R.id.txtworkQ1);
        txtMeetingQ1 = (TextView) v.findViewById(R.id.txtMeetingQ1);
        getActivity().setTitle("Q1");
        txtworkQ1.setOnClickListener(this);
        txtMeetingQ1.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtworkQ1:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q1Working(), Q1Working.class.getSimpleName());
                break;
            case R.id.txtMeetingQ1:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q1Meeting(), Q1Meeting.class.getSimpleName());
                break;
        }
    }
}
