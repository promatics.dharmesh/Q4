package com.q4.Q1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.q4.R;

/**
 * Created by promatics on 26/11/15.
 */
public class Q1MeetingDetermine extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q1_meeting_determine, container, false);
        getActivity().setTitle("Meeting with Q1");
        return v;
    }
}
