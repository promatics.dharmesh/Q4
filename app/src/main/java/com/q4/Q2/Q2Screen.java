package com.q4.Q2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.q4.R;
import com.q4.dashboard.Dashboard;

/**
 * Created by promatics on 26/11/15.
 */
public class Q2Screen extends Fragment implements View.OnClickListener {
    private TextView txtworkQ2, txtMeetingQ2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q2_screen, container, false);
        getActivity().setTitle("Q2");
        txtMeetingQ2 = (TextView) v.findViewById(R.id.txtMeetingQ2);
        txtworkQ2 = (TextView) v.findViewById(R.id.txtworkQ2);
        txtMeetingQ2.setOnClickListener(this);
        txtworkQ2.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtMeetingQ2:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q2Meeting(), Q2Meeting.class.getSimpleName());
                break;
            case R.id.txtworkQ2:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q2Working(), Q2Working.class.getSimpleName());
                break;
        }
    }
}
