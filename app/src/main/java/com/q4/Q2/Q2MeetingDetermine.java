package com.q4.Q2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.q4.R;

/**
 * Created by promatics on 26/11/15.
 */
public class Q2MeetingDetermine extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q2_meeting_determine, container, false);
        getActivity().setTitle("Meeting with Q2");
        return v;
    }
}
