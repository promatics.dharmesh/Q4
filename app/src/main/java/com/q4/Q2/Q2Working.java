package com.q4.Q2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.q4.R;
import com.q4.dashboard.Dashboard;

/**
 * Created by promatics on 26/11/15.
 */
public class Q2Working extends Fragment implements View.OnClickListener {
    private TextView txtActions, txtApproach, txtbarriers, txtNeeds, txtProbes;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q2_working, container, false);
        getActivity().setTitle("Working with Q2");
        txtActions = (TextView) v.findViewById(R.id.txtActions);
        txtApproach = (TextView) v.findViewById(R.id.txtApproach);
        txtbarriers = (TextView) v.findViewById(R.id.txtbarriers);
        txtNeeds = (TextView) v.findViewById(R.id.txtNeeds);
        txtProbes = (TextView) v.findViewById(R.id.txtProbes);
        txtActions.setOnClickListener(this);
        txtApproach.setOnClickListener(this);
        txtbarriers.setOnClickListener(this);
        txtNeeds.setOnClickListener(this);
        txtProbes.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtActions:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q2Actions(), Q2Actions.class.getSimpleName());
                break;
            case R.id.txtApproach:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q2Approach(), Q2Approach.class.getSimpleName());
                break;
            case R.id.txtbarriers:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q2Barriers(), Q2Barriers.class.getSimpleName());
                break;
            case R.id.txtNeeds:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q2Needs(), Q2Needs.class.getSimpleName());
                break;
            case R.id.txtProbes:
                // todo
                break;
        }
    }
}
