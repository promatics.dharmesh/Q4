package com.q4.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.q4.R;


public class CustomProgressDialog extends Dialog {
    Context mContext;
    SmoothProgressBar customizeProgress;
    TextView dialog_text;
    View v = null;

    public CustomProgressDialog(Context context) {
        super(context);
        mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.progress_bar_inflator);
        v = getWindow().getDecorView();
        v.setBackgroundResource(android.R.color.transparent);
        customizeProgress=(SmoothProgressBar)v.findViewById(R.id.progressbar);
        dialog_text=(TextView)v.findViewById(R.id.loading_text);
    }
}
