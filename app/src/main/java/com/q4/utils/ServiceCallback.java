package com.q4.utils;

import org.json.JSONObject;

/**
 * Created by android2 on 9/7/15.
 */
public interface ServiceCallback {

    public void onServiceResponse(int requestcode, JSONObject response);
  //  public void onServiceResponse(int requestcode,JSONArray jsonArray);

}
