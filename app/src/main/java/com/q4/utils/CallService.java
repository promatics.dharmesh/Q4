package com.q4.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


public class CallService extends AsyncTask<String, Void, String> {
    final int requestcode;
    HttpURLConnection httpURLConnection;
    ServiceCallback callback;
    Context ctx;
    CustomProgressDialog progressDialog;
    HashMap<String, String> postparams;
    String result = "";

    public CallService(ServiceCallback callback, Context ctx, int requestcode) {
        super();
        this.callback = callback;
        this.ctx = ctx;
        this.requestcode = requestcode;
    }

    public CallService(ServiceCallback callback, Context ctx, int requestcode, HashMap<String, String> postparams) {
        this(callback, ctx, requestcode);
        this.postparams = postparams;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new CustomProgressDialog(ctx);
        // progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        // progressDialog.setIndeterminate(true);
        progressDialog.show();

    }

    @Override
    protected String doInBackground(String... params) {
        final String url = params[0];
        Log.e("url is", url);
        Log.e("params are ", params.toString());
        try {
            URL url1 = new URL(url);
            httpURLConnection = (HttpURLConnection) url1.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(4500);
            httpURLConnection.connect();
            OutputStream os = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            //    bufferedWriter.write(getPostData(postparams));
            //  Log.e("values", getPostData(postparams));
            os.write(getPostData(postparams).getBytes());
            bufferedWriter.flush();
            bufferedWriter.close();

            os.close();
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream is = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder builder = new StringBuilder();
                String line = bufferedReader.readLine();
                while (line != null) {
                    builder.append(line + "\n");
                    line = bufferedReader.readLine();
                }
                Log.e("builder contains", builder.toString());
                is.close();
                result = builder.toString();
                return result;
            } else {
                result = "Invalid response from the server";
                return result;
            }

        } catch (MalformedURLException mfe) {
            mfe.printStackTrace();
            result = "Please check your internet connection";
            return result;
        } catch (IOException ioe) {
            result = "Please check your internet connection";
            ioe.printStackTrace();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            result = "Error occurred!";
            return result;
        } finally {
            httpURLConnection.disconnect();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (progressDialog.isShowing())
            progressDialog.dismiss();
        try {
            JSONObject jsonresponse = new JSONObject(result);
            Log.e("response are ", jsonresponse.toString());
            callback.onServiceResponse(requestcode, jsonresponse);
        } catch (JSONException e) {
            CommonUtils.showToast1(ctx, result);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtils.showToast1(ctx, "Error occurred!");
        }
    }

    private String getPostData(HashMap<String, String> postparams) {
        StringBuilder builder = new StringBuilder();
        if (postparams != null) {
            for (Map.Entry<String, String> entry : postparams.entrySet()) {
                try {
                    builder.append("&");
                    builder.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                    builder.append("=");
                    builder.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Log.e("params are ", builder.toString());
            return builder.toString();
        } else {
            return "";
        }

    }


}
