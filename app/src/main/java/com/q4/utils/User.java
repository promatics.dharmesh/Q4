package com.q4.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by promatics on 27/11/15.
 */
public class User {
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String fname) {
        Fname = fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    private String userId;
    private String Fname;
    private String Lname;
    private String email;
    private String mobNo;
    private String address;
    private String aboutMe;
    private SharedPreferences prefs;
    private static User instance = null;

    private User() {
        super();
        try {
            prefs = App.getInstance().getSharedPreferences(Constants.KEY_PREF, Context.MODE_PRIVATE);
            userId = prefs.getString("KEY_USER_ID", null);
            Fname = prefs.getString("KEY_FNAME", null);
            Lname = prefs.getString("KEY_LNAME", null);
            email = prefs.getString("KEY_EMAILID", null);
            mobNo = prefs.getString("KEY_MOB", null);
            address = prefs.getString("KEY_ADDRESS", null);
            aboutMe = prefs.getString("KEY_ABOUTME", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized User getInstance() {
        instance = new User();
        if (instance.userId != null)
            return instance;
        else
            return null;
    }

    public static void storeUserData(String userID, String Fname, String Lname, String emailId, String mobNo, String address, String aboutMe) {
        SharedPreferences.Editor editor = App.getInstance()
                .getSharedPreferences(Constants.KEY_PREF, Context.MODE_PRIVATE).edit();
        editor.putString("KEY_USER_ID", userID);
        editor.putString("KEY_FNAME", Fname);
        editor.putString("KEY_LNAME", Lname);
        editor.putString("KEY_EMAILID", emailId);
        editor.putString("KEY_MOB", mobNo);
        editor.putString("KEY_ADDRESS", address);
        editor.putString("KEY_ABOUTME", aboutMe);
        editor.apply();
    }

    public void logout() {
        try {
            App.getInstance().getSharedPreferences(Constants.KEY_PREF, Context.MODE_PRIVATE).edit()
                    .clear().commit();
            instance = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
