package com.q4.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

public final class CommonUtils {
    public static final SimpleDateFormat source = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    public static final SimpleDateFormat source1 = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
    public static final SimpleDateFormat target = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
    public static final SimpleDateFormat target1 = new SimpleDateFormat("dd-MMMM-yyyy", Locale.getDefault());
    public static final SimpleDateFormat target2 = new SimpleDateFormat("yyyy-MMM-dd", Locale.getDefault());

    public static void showToast(String msg) {
        Toast.makeText(App.getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToast1(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }


    public static String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] projection = {MediaStore.Images.Media.DATA};
            cursor = App.getContext().getContentResolver().query(contentUri, projection, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static Uri getTempFileUri() {
        File fileToStore = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
        return Uri.fromFile(fileToStore);
    }

    public static Uri getNewTempFileUri() {
        File fileToStore = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
        if (fileToStore.exists()) {
            fileToStore.delete();
            fileToStore = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
        }
        return Uri.fromFile(fileToStore);
    }

    public static void showDialog(Context cxt, String msg) {

        final AlertDialog.Builder alert = new AlertDialog.Builder(cxt);
        alert.setTitle("");
        alert.setMessage(msg);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        try {
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

}
