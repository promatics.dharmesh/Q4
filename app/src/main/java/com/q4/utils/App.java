package com.q4.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class App extends Application {
    private static App instance = null;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static synchronized App getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static SharedPreferences getGlobalPrefs() {
        return getContext().getSharedPreferences("APP_SETTINGS", 0);
    }
}
