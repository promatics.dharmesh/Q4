package com.q4.utils;

/**
 * Created by promatics on 27/11/15.
 */
public class Constants {

    public static final String KEY_PREF = "user_data";

    public static final String BASE = "http://mars.promaticstechnologies.com/Q4solution/mobile/";

    public static final String USER_SIGNIN = BASE + "signin";
    public static final int REQ_USER_SIGNIN = 10;

    public static final String USER_FORGOT_PASS = BASE + "forgotPassword";
    public static final int REQ_USER_FORGOT_PASS = 11;

    public static final String PRIVACY_POLICY = BASE + "privacy_policy";
    public static final int REQ_PRIVACY_POLICY = 12;

    public static final String ABOUT_US = BASE + "about_us";
    public static final int REQ_ABOUT_US = 13;

    public static final String CONTACT_US = BASE + "contact_us";
    public static final int REQ_CONTACT_US = 14;

    public static final String UPDATE_PROFILE = BASE + "edit_user_profile";
    public static final int REQ_UPDATE_PROFILE = 15;

}
