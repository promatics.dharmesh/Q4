package com.q4;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.q4.dashboard.Dashboard;
import com.q4.utils.CommonUtils;
import com.q4.utils.User;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            if (User.getInstance() != null) {
                Intent i = new Intent(this, Dashboard.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            } else {
                getSupportFragmentManager().beginTransaction().add(R.id.container, new SplashScreen()).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtils.showToast1(this, "main act Error Occurred!");
        }
    }
}
