package com.q4.Q3;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.q4.R;

/**
 * Created by promatics on 26/11/15.
 */
public class Q3Actions extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q3_actions, container, false);
        getActivity().setTitle("Working with Q3");
        return v;
    }
}
