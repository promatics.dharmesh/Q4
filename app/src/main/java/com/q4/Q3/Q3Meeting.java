package com.q4.Q3;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.q4.R;
import com.q4.dashboard.Dashboard;

/**
 * Created by promatics on 26/11/15.
 */
public class Q3Meeting extends Fragment implements View.OnClickListener {
    private TextView txtEstablish, txtGather, txtDeternmine, txtClarify, txtFacilitate;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q3_meeting, container, false);
        getActivity().setTitle("Meeting with Q3");
        txtEstablish = (TextView) v.findViewById(R.id.txtEstablish);
        txtGather = (TextView) v.findViewById(R.id.txtGather);
        txtDeternmine = (TextView) v.findViewById(R.id.txtDeternmine);
        txtClarify = (TextView) v.findViewById(R.id.txtClarify);
        txtFacilitate = (TextView) v.findViewById(R.id.txtFacilitate);
        txtEstablish.setOnClickListener(this);
        txtGather.setOnClickListener(this);
        txtDeternmine.setOnClickListener(this);
        txtClarify.setOnClickListener(this);
        txtFacilitate.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtEstablish:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q3MeetingEstablish(), Q3MeetingEstablish.class.getSimpleName());
                break;
            case R.id.txtGather:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q3MeetingGather(), Q3MeetingGather.class.getSimpleName());
                break;
            case R.id.txtDeternmine:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q3MeetingDetermine(), Q3MeetingDetermine.class.getSimpleName());
                break;
            case R.id.txtClarify:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q3MeetingClarify(), Q3MeetingClarify.class.getSimpleName());
                break;
            case R.id.txtFacilitate:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q3MeetingFaciliate(), Q3MeetingFaciliate.class.getSimpleName());
                break;
        }
    }
}

