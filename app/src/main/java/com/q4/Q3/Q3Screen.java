package com.q4.Q3;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.q4.R;
import com.q4.dashboard.Dashboard;

/**
 * Created by promatics on 26/11/15.
 */
public class Q3Screen extends Fragment implements View.OnClickListener {
    private TextView txtworkQ3, txtMeetingQ3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q3_screen, container, false);
        getActivity().setTitle("Q3");
        txtMeetingQ3 = (TextView) v.findViewById(R.id.txtMeetingQ3);
        txtworkQ3 = (TextView) v.findViewById(R.id.txtworkQ3);
        txtMeetingQ3.setOnClickListener(this);
        txtworkQ3.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtMeetingQ3:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q3Meeting(), Q3Meeting.class.getSimpleName());
                break;
            case R.id.txtworkQ3:
                ((Dashboard) getActivity()).beginTransactions(R.id.content_frame, new Q3Working(), Q3Working.class.getSimpleName());
                break;
        }
    }
}
