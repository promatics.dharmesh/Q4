package com.q4.Q3;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.q4.R;

public class Q3MeetingDetermine extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.q3_meeting_determine, container, false);
        getActivity().setTitle("Meeting with Q3");
        return v;
    }
}
