package com.q4.login;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.q4.R;
import com.q4.dashboard.Dashboard;
import com.q4.utils.CallService;
import com.q4.utils.CommonUtils;
import com.q4.utils.Constants;
import com.q4.utils.ServiceCallback;
import com.q4.utils.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by promatics on 26/11/15.
 */
public class LoginScreen extends Fragment implements View.OnClickListener, ServiceCallback {
    private TextView txtForgotpass;
    private Button btnLogin;
    private EditText etEmail, etPass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_screen, container, false);
        txtForgotpass = (TextView) v.findViewById(R.id.txtForgotPass);
        btnLogin = (Button) v.findViewById(R.id.btnLogin);
        etEmail = (EditText) v.findViewById(R.id.etEmail);
        etPass = (EditText) v.findViewById(R.id.etPass);
        txtForgotpass.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtForgotPass:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, new ForgotPassword()).addToBackStack("").commit();
                break;
            case R.id.btnLogin:
                if (!etEmail.getText().toString().matches(Patterns.EMAIL_ADDRESS.pattern())) {
                    etEmail.setError("Please enter your valid email Address");
                    etEmail.requestFocus();
                } else if (etPass.getText().toString().trim().length() == 0) {
                    etPass.setError("Please enter your password");
                    etPass.requestFocus();
                } else {
                    etEmail.setError(null);
                    etPass.setError(null);
                    LogIn();
                }
                break;
        }
    }

    private void LogIn() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", etEmail.getText().toString());
        map.put("login", "login");
        map.put("password", etPass.getText().toString());
        new CallService(this, getActivity(), Constants.REQ_USER_SIGNIN, map).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.USER_SIGNIN);
    }

    @Override
    public void onServiceResponse(int requestcode, JSONObject response) {
        switch (requestcode) {
            case Constants.REQ_USER_SIGNIN:
                try {
                    int code = response.getInt("status");
                    if (code == 1) {
                        JSONObject obj = response.optJSONObject("member_data");
                        String userId = obj.optString("id");
                        String fname = obj.optString("firstname");
                        String lname = obj.optString("lastname");
                        String address = obj.optString("address");
                        String phone = obj.optString("phone");
                        String email = obj.optString("email");
                        String about_me = obj.optString("about_me");
                        User.storeUserData(userId, fname, lname, email, phone, address, about_me);
                        Intent i = new Intent(getActivity(), Dashboard.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        String msg = response.getString("msg");
                        CommonUtils.showDialog(getActivity(), msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showToast1(getActivity(), "Invalid response from server");
                }
                break;
        }
    }
}
