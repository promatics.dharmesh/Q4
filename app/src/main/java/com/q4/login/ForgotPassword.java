package com.q4.login;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.q4.R;
import com.q4.utils.CallService;
import com.q4.utils.CommonUtils;
import com.q4.utils.Constants;
import com.q4.utils.ServiceCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by promatics on 26/11/15.
 */
public class ForgotPassword extends Fragment implements View.OnClickListener, ServiceCallback {
    private EditText etEmail;
    private Button btnSubmit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.forgot_screen, container, false);
        etEmail = (EditText) v.findViewById(R.id.etEmail);
        btnSubmit = (Button) v.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        if (!etEmail.getText().toString().matches(Patterns.EMAIL_ADDRESS.pattern())) {
            etEmail.setError("Please enter your valid email addresss");
            etEmail.requestFocus();
        } else {
            etEmail.setError(null);
            ForgotPass();
        }
    }

    private void ForgotPass() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", etEmail.getText().toString());
        map.put("submit", "forgot");
        new CallService(this, getActivity(), Constants.REQ_USER_FORGOT_PASS, map).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constants.USER_FORGOT_PASS);
    }

    @Override
    public void onServiceResponse(int requestcode, JSONObject response) {
        switch (requestcode) {
            case Constants.REQ_USER_FORGOT_PASS:
                try {
                    int code = response.getInt("status");
                    if (code == 1) {
                        String msg = response.getString("msg");
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(R.string.app_name);
                        builder.setMessage(msg);
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().onBackPressed();
                            }
                        });
                        builder.show();
                    } else {
                        String msg = response.getString("msg");
                        CommonUtils.showDialog(getActivity(), msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showToast1(getActivity(), "Invalid response drom server");
                }
                break;
        }
    }
}
